package com.eastsunsoft.swordfish.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * <h1>SwordfishEurekaApp</h1>
 * </br>
 *
 * @author donny
 * @date 11/09/2017
 */
@EnableEurekaServer
@SpringBootApplication
public class SwordfishEurekaApp {
    public static void main(String[] args) {
        SpringApplication.run(SwordfishEurekaApp.class, args);
    }
}
