# swordfish-cloud
剑鱼云平台
==========

# 1. Swodfish Cloud平台简介

基于SpringCloud微服务敏捷开发系统架构，提供整套公共微服务服务模块：内容管理、支付中心、用户管理（包括第三方）、微信平台、存储系统、配置中心、日志分析等，支持服务治理、监控和追踪等功能。

# 2. 组织结构

```
swordfish-cloud
├── swordfish-common // 公共模块
├── swordfish-config-server // 服务统一配置中心 [端口:6006]
├── swordfish-eureka // 服务注册发现中心 [端口:6001]
├── swordfish-gateway // 微服务网关 [端口:5888]
├── swordfish-hystrix-dashboard // 断路器监控中心 [端口:6002]
├── swordfish-hystrix-turbine // 断路器数据统一聚合服务 [端口:6003]
├── swordfish-zipkin-server // 微服务跟踪系统 [端口:6007]
├── swordfish-ucms // 用户权限管理系统
|    ├── swordfish-ucms-service // ucms微服务单元
├── swordfish-cms -- // 内容管理系统
├── swordfish-sms -- // 短信系统
|    ├── swordfish-sms-service // 短信微服务单元
├── swordfish-pay -- // 支付系统
|    ├── swordfish-pay-service // pay微服务单元
├── swordfish-wechat // 微信系统
├── swordfish-oss // 对象存储系统
|    ├── swordfish-oss-service // 开发工具包
├── swordfish-shop // 电子商务系统

```

# 3. 技术选型

## 3.1 后端技术

| 技术                         | 名称              | 官网                                       |
| -------------------------- | --------------- | ---------------------------------------- |
| Spring Cloud               | 微服务框架           | <http://projects.spring.io/spring-cloud/> |
| Spring Cloud Config        | 统一配置            | <https://cloud.spring.io/spring-cloud-static/spring-cloud-config/1.3.3.RELEASE/> |
| Spring Cloud Bus           | 事件总线            | <https://cloud.spring.io/spring-cloud-static/spring-cloud-bus/1.3.1.RELEASE/> |
| Netflix Eureka             | 服务发现注册中心组建      | <https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.3.5.RELEASE/> |
| Netflix Hystrix            | 微服务容错组建         | <https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.3.5.RELEASE/> |
| Netflix Zuul               | 微服务网关组件         | <https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.3.5.RELEASE/> |
| Spring Cloud Sleuth        | 微服务日志组件         | <https://cloud.spring.io/spring-cloud-static/spring-cloud-sleuth/1.2.5.RELEASE/> |
| Spring Cloud Stream        | 数据流操作开发包        | <https://docs.spring.io/spring-cloud-stream/docs/Ditmars.RELEASE/reference/htmlsingle/> |
| Spring Boot                | Web快速开发框架       | <https://docs.spring.io/spring-boot/docs/1.5.8.RELEASE/reference/htmlsingle/> |
| SpringMVC                  | MVC框架           | <http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc> |
| jwt                        | 无状态分布式安全技术      | <https://jwt.io/>                        |
| MyBatis                    | ORM框架           | <http://www.mybatis.org/mybatis-3/zh/index.html> |
| tkmapper                   | MyBatis通用Mapper | <https://gitee.com/free/Mapper>          |
| PageHelper                 | MyBatis物理分页插件   | <https://gitee.com/free/Mybatis_PageHelper> |
| Druid                      | 数据库连接池          | <https://github.com/alibaba/druid>       |
| FluentValidator            | 校验框架            | <https://github.com/neoremind/fluent-validator> |
| freemarker                 | 模板引擎            | <http://www.thymeleaf.org/>              |
| Redis                      | 分布式缓存数据库        | <https://redis.io/>                      |
| Elasticsearch              | 分布式全文搜索引擎       | <http://lucene.apache.org/solr/><https://www.elastic.co/> |
| RabbitMQ                   | 消息队列            | <http://www.rabbitmq.com/>               |
| Log4J                      | 日志组件            | <http://logging.apache.org/log4j/1.2/>   |
| Swagger2                   | 接口测试框架          | <http://swagger.io/>                     |
| AliOSS & Qiniu & QcloudCOS | 云存储             | <https://www.aliyun.com/product/oss/><http://www.qiniu.com/><https://www.qcloud.com/product/cos> |
| jackson & json             | 数据序列化           | <https://github.com/FasterXML/jackson>   |
| Jenkins                    | 持续集成工具          | <https://jenkins.io/index.html>          |
| Maven                      | 项目构建管理          | <http://maven.apache.org/>               |

## 3.2 前端技术

| 技术                          | 名称           | 官网                                       |
| --------------------------- | ------------ | ---------------------------------------- |
| vue                         | 渐进式前端框架      | <https://cn.vuejs.org/>                  |
| vue-router                  | 前端路由         | <https://router.vuejs.org/zh-cn/>        |
| vuex                        | 状态管理         | <https://vuex.vuejs.org/zh-cn/>          |
| iview                       | 基于vue前端ui库   | <https://www.iviewui.com/>               |
| axios                       | http client库 | <https://github.com/axios/axios>         |
| webpack                     | 前端构建工具       | <http://webpack.github.io/>              |
| nodejs                      | js运行环境       | <https://nodejs.org/en/>                 |
| Font-awesome                | 字体图标         | <http://fontawesome.io/>                 |
| material-design-iconic-font | 字体图标         | <https://github.com/zavoloklom/material-design-iconic-font> |

# 4. 微服务单元介绍

## 4.1 swordfish-common

## 4.2 swordfish-config-server

统一配置服务中心，用于对所有微服务配置进行统一管理

## 4.3 swordfish-eureka

服务发现注册中心，用于所有微服务的注册发现

## 4.4 swordfish-gateway

微服务网关，是进入微服务的唯一入口，提供身份验证授权、服务路由、负载均衡等功能

## 4.5 swordfish-hystrix-dashboard

微服务断路器监控面板，用了监控所有微服务的断路器状态信息

## 4.6 swordfish-hystrix-turbine

断路器聚合服务，用来收集所有微服务产生的断路器日志

## 4.7 swordfish-zipkin-server

微服务调用日志跟踪服务，可以查看api调用日志信息、时间信息、服务依赖关系等

## 4.8 swordfish-ucms

用户管理中心，微服务子系统之一，用于统一管理系统用户信息

## 4.9 swordfish-cms

内容管理系统，微服务子系统之一，提供内容管理服务

## 4.9 swordfish-sms

短信服务，微服务单元之一，提供短信发送、审计、计费、日志等服务

## 4.10 swordfish-pay

支付服务，微服务子系统之一，用于提高统一的支付接口

## 4.11 swordfish-wechat

微信服务，微服务单元之一，提供与微信平台的对接

## 4.12 swordfish-oss

对象存储服务，微服务单元之一，提供与主流云存储服务商平台对接

## 4.13 swordfish-shop

电子商城服务，微服务子系统之一，提供电子商城相关服务

# 5. 开发工具&环境介绍

## 5.1 开发工具

- IntelliJ IDEA: 开发IDE
- PowerDesigner: 建模工具
- Navicat for MySQL: 数据库客户端
- MySql: 数据库
- Redis: 内存数据库
- RabbitMQ: 消息队列中间件
- Git: 版本管理
- Nginx: 反向代理服务器
- Varnish: HTTP加速器

## 5.2 开发环境

- Jdk8+
- Mysql5.7+
- Redis
- Eureka
- RabbitMQ

# 6. 生产拓扑图

[![img](http://7xnoo1.com1.z0.glb.clouddn.com/donnyblog/swordfish/03-09.png)](http://7xnoo1.com1.z0.glb.clouddn.com/donnyblog/swordfish/03-09.png)

# 7. 开发进度

后续补充...

# 8. 其他问题

待补充...