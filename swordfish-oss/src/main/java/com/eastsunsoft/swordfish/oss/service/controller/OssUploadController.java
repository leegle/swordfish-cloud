package com.eastsunsoft.swordfish.oss.service.controller;

import com.eastsunsoft.swordfish.common.ResJson;
import com.eastsunsoft.swordfish.common.SwordfishException;
import com.eastsunsoft.swordfish.oss.service.common.OssConstant;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * <h1>OssUploadController</h1>
 * </br>
 *
 * @author donny
 * @date 22/09/2017
 */
@RestController
@RequestMapping("/api")
public class OssUploadController {
    @PostMapping("/upload")
    public ResJson uploadFile(@RequestParam(value = "file") MultipartFile file) {
        ResJson resJson = new ResJson();
        try {
            byte[] bytes = file.getBytes();
            File fileToSave = new File(file.getOriginalFilename());
            FileCopyUtils.copy(bytes, fileToSave);
            resJson.setBody(fileToSave.getAbsolutePath());
        } catch (Exception e) {
            throw new SwordfishException(OssConstant.ERR_USER_NOT_MATCH);
        }
        return resJson;
    }

    @GetMapping("/test")
    public ResJson test() {
        return new ResJson();
    }
}
