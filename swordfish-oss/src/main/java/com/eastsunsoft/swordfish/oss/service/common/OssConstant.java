package com.eastsunsoft.swordfish.oss.service.common;

import com.eastsunsoft.swordfish.common.ApiConstant;

/**
 * <h1>OssConstant</h1>
 * </br>
 *
 * @author donny
 * @date 22/09/2017
 */
public class OssConstant {
    public static final Integer ERRCODE_UPLOAD_FAILED = 30001;
    public static final String ERRMSG_UPLOAD_FAILED = "uplaod file failed!";
    public static final String ERR_USER_NOT_MATCH = ERRCODE_UPLOAD_FAILED + ApiConstant.JOINSTR + ERRMSG_UPLOAD_FAILED;

    private OssConstant() {
        throw new IllegalAccessError("Utility class");
    }

}
