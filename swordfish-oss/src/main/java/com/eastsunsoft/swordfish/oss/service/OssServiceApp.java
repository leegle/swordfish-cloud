package com.eastsunsoft.swordfish.oss.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 22/09/2017
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OssServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(OssServiceApp.class, args);
    }
}
