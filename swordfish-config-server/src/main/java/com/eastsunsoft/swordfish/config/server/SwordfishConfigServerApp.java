package com.eastsunsoft.swordfish.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * <h1>SwordfishConfigServerApp</h1>
 * </br>
 *
 * @author donny
 * @date 22/09/2017
 */
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class SwordfishConfigServerApp {
    // TODO Dalston SR3 jce bug fix?
    public static void main(String[] args) {
        SpringApplication.run(SwordfishConfigServerApp.class, args);
    }
}
