package com.eastsunsoft.swordfish.common;



import java.io.Serializable;


/**
 * ResJson
 *
 * @author donny
 * @date 8/31/16
 */

public class ResJson implements Serializable {
    private static final long serialVersionUID = -6651196247498156422L;
    private Integer errcode = ApiConstant.CODE_SUCCESS;
    private String msg = "";
    private Object body = "";

    public ResJson() {
    }

    public ResJson(Integer errcode, String msg) {
        this.errcode = errcode;
        this.msg = msg;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "ResJson{" +
                "errcode=" + errcode +
                ", msg='" + msg + '\'' +
                ", body=" + body +
                '}';
    }
}
