package com.eastsunsoft.swordfish.common;

/**
 * <h1>ServiceConstant</h1>
 * </br>
 *
 * @author donny
 * @date 15/09/2017
 */
public class ServiceConstant {
    private ServiceConstant() {
        throw new IllegalAccessError("Utility class");
    }

    public final static String UCMS = "swordfish-ucms";
    public final static String OSS = "swordfish-oss";

}
