package com.eastsunsoft.swordfish.common;

/**
 * <h1>KpsoException</h1>
 * </br>
 *
 * @author donny
 * @date 10/08/2017
 */
public class SwordfishException extends RuntimeException {
    private static final long serialVersionUID = -4446318227301471010L;

    public SwordfishException(String message) {
        super(message);
    }
}
