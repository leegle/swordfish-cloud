package com.eastsunsoft.swordfish.common;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 10/08/2017
 */
public class ApiConstant {


    private ApiConstant() {
        throw new IllegalAccessError("Utility class");
    }

    public static final String STRING_TRUE = "true";
    public static final Integer CODE_SUCCESS = 0;
    public static final Integer YES = 1;
    public static final Integer NO = 0;
    public static final Integer DEFAULT_PAGE_NO = 1;
    public static final Integer DEFAULT_PAGE_SIZE = 10;
    public static final String JOINSTR = ":";

    public static final String PARAM_APIKEY = "api_key";
    public static final String PARAM_ACCESSTOKEN = "accessToken";
    public static final String PARAM_TIMESTAMP = "timestamp";


    public static final String ERRCODE_DUPLICATEKEY = "90001";
    public static final String ERRMSG_DUPLICATEKEY = "Record Exists!";

    public static final String ERRMSG_INVALID_HEADER = "invalid header!";
    public static final String ERRMSG_UNAUTHORIZED = "Unauthorized!";
    public static final String ERRMSG_INVALID_TOKEN = "invald token!";
    public static final String ERRMSG_ACCESS_DENY = "Access Denied!";

    public static final int ERRCODE_ACCESSTOKEN_INVALID_INT = 42001;
    public static final String ERRMSG_ACCESSTOKEN_MISSING = "AccessToken is missing";
    public static final String ERRCODE_TIMESTAMP_INVALID = "Timestamp invalid";
    public static final String ERRCODE_NOT_LOGIN = "User Not Login!";
    public static final String ERRMSG_ACCESSTOKEN_INVALID = "AccessToken is invalid!";


    public static final int ERRCODE_SERVICE_TEMPDOWN = 99999;
    public static final String ERRMSG_SERVICE_TEMPDOWN = "Service temporarily unavailable!";
    public static final String ERR_SERVICE_TEMPDOWN = ERRCODE_SERVICE_TEMPDOWN + ":" +  ERRMSG_SERVICE_TEMPDOWN;

    public static final int ERRCODE_ACCESSTOKEN_EXPIRED = 42002;
    public static final String ERRMSG_ACCESSTOKEN_EXPIRED = "AccessToken is expired!";
    public static final String ERR_ACCESSTOKEN_EXPIRED = ERRCODE_ACCESSTOKEN_EXPIRED + ":" +  ERRMSG_ACCESSTOKEN_EXPIRED;


    public static final int ERRCODE_NO_PERMISSION  = 60002;
    public static final String ERRMSG_NO_PERMISSION = "No Permission!";
    public static final String ERR_NO_PERMISSION = ERRCODE_NO_PERMISSION + ":" + ERRMSG_NO_PERMISSION;

    public static final String ERRCODE_PARAMS_INVALID = "40035:Invalid Params!";

    public static final String ERRCODE_SMS_SEND_FAILED = "500001:Send SMS failed!";
    public static final String ERRCODE_VERIFI_CODE_ALREAYSEND = "500002:Verify Code Already Send!";
    public static final String ERRCODE_VERIFI_CODE_FAILED = "500003:Verify Code Failed!";
    public static final String ERRCODE_BIND_EXISTS = "500004:User Already Bind!";



    public static final String ERRCODE_OLDPWD_NOT_MATCH = "60001:Old Pwd Not Matched!";

    public static final String ERRCODE_RECORD_EXISTS = "90001:Record Exists!";
    public static final String ERRCODE_RECORD_NOT_EXISTS = "90002:Record Not Exists!";
    public static final String ERRCODE_PRODLISTNO_UNCHECK = "90003:ProdListNo Uncheck!";
    public static final String ERRCODE_BARCODE_NOT_MATCHED = "90004:Barcode Not Matched!";

    public static final String ERRCODE_DAO = "99991";
    public static final String ERRCODE_EXCEPTION = "99998";
    public static final Integer ERRCODE_EXCEPTION_INT = 99998;
    public static final String ERRCODE_INIT_DATABASE = "99995: init database error";
    public static final String ERRCODE_500 = "99996: 500 error!";
    public static final String ERRCODE_NOT_HANDLER_FOUND = "99997:Not handler found!";

    public static final String ERRCODE_FILEUPLOAD_FAILED = "9001010:File Upload Failed!";
}
