package com.eastsunsoft.swordfish.util;

import org.springframework.cglib.beans.BeanCopier;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>SwBeanUtil</h1>
 * </br>
 *
 * @author donny
 * @date 18/09/2017
 */
public class SwBeanUtil {
    public static Map<String, BeanCopier> beanCopierMap = new HashMap<>();

    /**
     * 属性复制
     *
     * @param source 资源类
     * @param target 目标类
     */
    public static void copyProperties(Object source, Object target) {
        String beanKey = generateKey(source.getClass(), target.getClass());
        BeanCopier copier = null;
        if (!beanCopierMap.containsKey(beanKey)) {
            copier = BeanCopier.create(source.getClass(), target.getClass(), false);
            beanCopierMap.put(beanKey, copier);
        } else {
            copier = beanCopierMap.get(beanKey);
        }
        copier.copy(source, target, null);
    }

    private static String generateKey(Class<?> class1, Class<?> class2) {
        return class1.toString() + class2.toString();
    }
}
