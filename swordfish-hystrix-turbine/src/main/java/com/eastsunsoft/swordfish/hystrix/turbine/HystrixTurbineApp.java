package com.eastsunsoft.swordfish.hystrix.turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.stream.EnableTurbineStream;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 22/09/2017
 */
@SpringBootApplication
@EnableTurbineStream
public class HystrixTurbineApp {
    public static void main(String[] args) {
        SpringApplication.run(HystrixTurbineApp.class, args);
    }
}
