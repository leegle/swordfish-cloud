package com.eastsunsoft.swordfish.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * <h1>SwordfishGatewayApp</h1>
 * </br>
 *
 * @author donny
 * @date 11/09/2017
 */
@EnableZuulProxy
@EnableFeignClients
@SpringCloudApplication
public class SwordfishGatewayApp {
    public static void main(String[] args) {
        SpringApplication.run(SwordfishGatewayApp.class, args);
    }
}
