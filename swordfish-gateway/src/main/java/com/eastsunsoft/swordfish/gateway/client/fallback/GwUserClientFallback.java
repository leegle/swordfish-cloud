package com.eastsunsoft.swordfish.gateway.client.fallback;

import com.eastsunsoft.swordfish.common.ApiConstant;
import com.eastsunsoft.swordfish.common.ResJson;
import com.eastsunsoft.swordfish.gateway.client.GwUserClient;
import org.springframework.stereotype.Component;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 14/09/2017
 */
@Component
public class GwUserClientFallback implements GwUserClient {
    @Override
    public ResJson auth(String authToken, String uri, String method) {
        return buildServiceTempUnavailable();
    }

    private ResJson buildServiceTempUnavailable() {
        ResJson resJson = new ResJson();
        resJson.setErrcode(ApiConstant.ERRCODE_SERVICE_TEMPDOWN);
        resJson.setMsg(ApiConstant.ERRMSG_SERVICE_TEMPDOWN);
        return resJson;
    }

}
