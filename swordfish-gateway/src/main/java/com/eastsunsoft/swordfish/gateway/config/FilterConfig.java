package com.eastsunsoft.swordfish.gateway.config;

import com.eastsunsoft.swordfish.gateway.filter.AccessFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <h1>FilterConfig</h1>
 * </br>
 *
 * @author donny
 * @date 11/09/2017
 */
@Configuration
public class FilterConfig {
    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }
}
