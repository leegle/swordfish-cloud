package com.eastsunsoft.swordfish.gateway.client;

import com.eastsunsoft.swordfish.common.ResJson;
import com.eastsunsoft.swordfish.common.ServiceConstant;
import com.eastsunsoft.swordfish.gateway.client.fallback.GwUserClientFallback;
import com.eastsunsoft.swordfish.gateway.config.FeignLogConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 17/09/2017
 */
@FeignClient(name = ServiceConstant.UCMS, configuration = FeignLogConfig.class, fallback = GwUserClientFallback.class)
@Qualifier("gwUserClient")
public interface GwUserClient {
    @GetMapping(value = "/user/auth", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResJson auth(@RequestParam("authToken") String authToken, @RequestParam("uri") String uri, @RequestParam("method") String method);
}