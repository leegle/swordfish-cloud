package com.eastsunsoft.swordfish.gateway.client.vo;

/**
 * <h1>UserReqVO</h1>
 * </br>
 *
 * @author donny
 * @date 15/09/2017
 */
public class UserReqVO {
    private Long id;
    private String username;
    private String userpwd;
    private Integer age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserReqVO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", userpwd='" + userpwd + '\'' +
                ", age=" + age +
                '}';
    }
}
