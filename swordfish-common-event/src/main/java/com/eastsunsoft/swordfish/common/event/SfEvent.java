package com.eastsunsoft.swordfish.common.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

/**
 * <h1>SfEvent</h1>
 * </br>
 * 消息接口
 *
 * @author donny
 * @date 19/09/2017
 */
@Component
public interface SfEvent {
    public final static String INPUT_COMMON = "inputCommon";
    public final static String OUT_COMMON = "outCommon";

    @Input(SfEvent.INPUT_COMMON)
    SubscribableChannel inputCommon();

    @Output(SfEvent.OUT_COMMON)
    MessageChannel outputCommon();
}
