package com.eastsunsoft.swordfish.common.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 19/09/2017
 */
@Component
public interface SfEventInputCommon {
    public final static String INPUT_COMMON = "inputCommon";

    @Input(SfEvent.INPUT_COMMON)
    SubscribableChannel inputCommon();
}
