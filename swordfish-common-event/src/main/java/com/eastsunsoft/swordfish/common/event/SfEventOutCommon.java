package com.eastsunsoft.swordfish.common.event;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

/**
 * <h1>SfEventOutCommon</h1>
 * </br>
 *
 * @author donny
 * @date 19/09/2017
 */
@Component
public interface SfEventOutCommon {
    public final static String OUT_COMMON = "outCommon";

    @Output(SfEvent.OUT_COMMON)
    MessageChannel outputCommon();
}
