package com.eastsunsoft.swordfish.ucms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

/**
 * <h1>RedisConfig</h1>
 * </br>
 *
 * @author donny
 * @date 18/09/2017
 */
@Configuration
public class RedisConfig {
    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public RedisTemplate<String, Map<String, String>> stringMapRedisTemplate() {
        RedisTemplate<String, Map<String, String>> stringMapRedisTemplate = new RedisTemplate<>();
        stringMapRedisTemplate.setConnectionFactory(redisConnectionFactory);
        stringMapRedisTemplate.setKeySerializer(stringMapRedisTemplate.getStringSerializer());
        stringMapRedisTemplate.setHashKeySerializer(stringMapRedisTemplate.getStringSerializer());
        stringMapRedisTemplate.setHashValueSerializer(stringMapRedisTemplate.getStringSerializer());
        stringMapRedisTemplate.afterPropertiesSet();

        return stringMapRedisTemplate;
    }
}
