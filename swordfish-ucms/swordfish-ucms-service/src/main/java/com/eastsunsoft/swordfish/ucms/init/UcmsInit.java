package com.eastsunsoft.swordfish.ucms.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 18/09/2017
 */
@Component
public class UcmsInit implements CommandLineRunner {
    @Autowired
    private RedisTemplate<String, Map<String, String>> stringMapRedisTemplate;

    @Override
    public void run(String... args) throws Exception {
        // 缓存权限信息
        Map<String, String> map = new HashMap<>();
        map.put("swordfish-ucms/testa:get", "UCMS_TESTA:R");
        map.put("swordfish-oss/api/upload:post", "OSS_UPLOAD");
        stringMapRedisTemplate.opsForHash().putAll("ucmsRoles", map);
    }
}
