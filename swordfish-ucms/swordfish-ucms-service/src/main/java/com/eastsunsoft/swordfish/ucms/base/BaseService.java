package com.eastsunsoft.swordfish.ucms.base;

/**
 * BaseService
 *
 * @author donny
 * @date 03/12/2016
 */

public interface BaseService<T> {
    T get(Object id);

    <S extends T> S save(S entity);
}
