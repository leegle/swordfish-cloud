package com.eastsunsoft.swordfish.ucms.mapper;

import com.eastsunsoft.swordfish.ucms.entity.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * <h1>UserMapper</h1>
 * </br>
 *
 * @author donny
 * @date 17/09/2017
 */
@Repository
public interface UserMapper extends Mapper<User> {
}
