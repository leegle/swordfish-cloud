package com.eastsunsoft.swordfish.ucms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <h1>SwordfishUcmsApp</h1>
 * </br>
 * 用户中心管理系统
 *
 * @author donny
 * @date 11/09/2017
 */
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.eastsunsoft.swordfish.ucms.mapper"})
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = {"com.eastsunsoft.swordfish.ucms", "com.eastsunsoft.swordfish.common"})
public class SwordfishUcmsApp {
    public static void main(String[] args) {
        SpringApplication.run(SwordfishUcmsApp.class, args);
    }
}
