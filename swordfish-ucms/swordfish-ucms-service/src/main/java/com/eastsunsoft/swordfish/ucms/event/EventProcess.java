package com.eastsunsoft.swordfish.ucms.event;

import com.eastsunsoft.swordfish.common.event.SfEventInputCommon;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * <h1>EventProcess</h1>
 * </br>
 *
 * @author donny
 * @date 19/09/2017
 */
@Component
@EnableBinding(SfEventInputCommon.class)
public class EventProcess {
    @StreamListener(SfEventInputCommon.INPUT_COMMON)
    public void input(Message<String> message) {
        System.out.println("一般监听收到：" + message.getPayload());
    }
}
