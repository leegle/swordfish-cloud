package com.eastsunsoft.swordfish.ucms.common;

import com.eastsunsoft.swordfish.common.ApiConstant;

/**
 * <h1>UcmsConstant</h1>
 * </br>
 *
 * @author donny
 * @date 18/09/2017
 */
public class UcmsConstant {
    public static final Integer ERRCODE_USER_NOT_MATCH = 20001;
    public static final String ERRMSG_USER_NOT_MATCH = "User not match!";
    public static final String ERR_USER_NOT_MATCH = ERRCODE_USER_NOT_MATCH + ApiConstant.JOINSTR + ERRMSG_USER_NOT_MATCH;
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_COMPANY_ADMIN = "ROLE_COMPANY_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String CLAIM_ROLE = "role";
    public static final String PREFIX_USER = "user:";
    public static final String USER_LOGIN_DATETIME = "loginDateTime";
    public static final String UCMS_ROLES = "ucmsRoles";

    private UcmsConstant() {
        throw new IllegalAccessError("Utility class");
    }

}
