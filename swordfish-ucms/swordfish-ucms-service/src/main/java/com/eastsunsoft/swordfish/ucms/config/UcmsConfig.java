package com.eastsunsoft.swordfish.ucms.config;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * <h1>UcmsConfig</h1>
 * </br>
 *
 * @author donny
 * @date 18/09/2017
 */
@Configuration
@ConfigurationProperties(prefix = "ucms")
@Validated
public class UcmsConfig {
    @Valid
    private final Jwt jwt = new Jwt();

    public Jwt getJwt() {
        return jwt;
    }

    public static class Jwt {
        @NotEmpty
        private String security;

        public String getSecurity() {
            return security;
        }

        public void setSecurity(String security) {
            this.security = security;
        }
    }
}
