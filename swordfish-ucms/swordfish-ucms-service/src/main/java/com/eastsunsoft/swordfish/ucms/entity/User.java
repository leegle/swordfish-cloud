package com.eastsunsoft.swordfish.ucms.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <h1>User</h1>
 * </br>
 *
 * @author donny
 * @date 15/09/2017
 */
@Table(name = "sw_ucms_user")
public class User {
    @Id
    private Long id;

    @Column(name = "company_id")
    private Long companyId;

    @Column(name = "depart_id")
    private Long departId;

    @Column(name = "account")
    private String account;

    @Column(name = "account")
    private String username;

    @Column(name = "userpwd")
    private String userpwd;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "email")
    private String email;

    @Column(name = "age")
    private Integer age;

    @Column(name = "is_admin")
    private Integer admin;

    @Column(name = "is_company_admin")
    private Integer companyAdmin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getDepartId() {
        return departId;
    }

    public void setDepartId(Long departId) {
        this.departId = departId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAdmin() {
        return admin;
    }

    public void setAdmin(Integer admin) {
        this.admin = admin;
    }

    public Integer getCompanyAdmin() {
        return companyAdmin;
    }

    public void setCompanyAdmin(Integer companyAdmin) {
        this.companyAdmin = companyAdmin;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", companyId=" + companyId +
                ", departId=" + departId +
                ", account='" + account + '\'' +
                ", username='" + username + '\'' +
                ", userpwd='" + userpwd + '\'' +
                ", fullname='" + fullname + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", admin=" + admin +
                ", companyAdmin=" + companyAdmin +
                '}';
    }
}
