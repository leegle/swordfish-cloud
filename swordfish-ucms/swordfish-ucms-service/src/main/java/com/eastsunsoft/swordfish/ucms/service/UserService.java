package com.eastsunsoft.swordfish.ucms.service;

import com.eastsunsoft.swordfish.ucms.base.BaseService;
import com.eastsunsoft.swordfish.ucms.dto.UserDTO;
import com.eastsunsoft.swordfish.ucms.entity.User;

import java.util.List;

/**
 * <h1>UserService</h1>
 * </br>
 *
 * @author donny
 * @date 17/09/2017
 */
public interface UserService extends BaseService<User> {
    /**
     * 根据账号和密码获取用户信息，账号支持account、mobile、email
     *
     * @param account 账号
     * @param pwd     密码
     * @return UserDTO 用户信息
     */
    UserDTO getByAccountAndPwd(String account, String pwd);

    /**
     * 根据ID查询用户权限信息
     *
     * @param id 主键
     * @return List<String>
     */
    List<String> listRolesByID(Long id);
}
