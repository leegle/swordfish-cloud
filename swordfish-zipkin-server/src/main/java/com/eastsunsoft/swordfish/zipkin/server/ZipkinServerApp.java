package com.eastsunsoft.swordfish.zipkin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;

/**
 * <h1>ZipkinServerApp</h1>
 * </br>
 *
 * @author donny
 * @date 20/10/2017
 */
@SpringBootApplication
@EnableZipkinStreamServer
public class ZipkinServerApp {
    public static void main(String[] args) {
        SpringApplication.run(ZipkinServerApp.class, args);
    }
}
