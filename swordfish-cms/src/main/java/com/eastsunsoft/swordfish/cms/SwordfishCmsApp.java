package com.eastsunsoft.swordfish.cms;

import com.eastsunsoft.swordfish.common.event.SfEventOutCommon;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <h1>SwordfishCmsApp</h1>
 * </br>
 *
 * @author donny
 * @date 14/09/2017
 */

@SpringBootApplication(scanBasePackages = {"com.eastsunsoft.swordfish.cms", "com.eastsunsoft.swordfish.common"})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableFeignClients
@EnableBinding(SfEventOutCommon.class)
@MapperScan(basePackages = {"com.eastsunsoft.swordfish.cms.mapper"})
@EnableTransactionManagement
@EnableAsync
public class SwordfishCmsApp {
    public static void main(String[] args) {
        SpringApplication.run(SwordfishCmsApp.class, args);
    }
}
