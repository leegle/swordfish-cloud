package com.eastsunsoft.swordfish.cms.mapper;

import com.eastsunsoft.swordfish.cms.entity.CmsArticle;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 * <h1>CmsArticleMapper</h1>
 * </br>
 *
 * @author donny
 * @date 20/09/2017
 */
@Repository
public interface CmsArticleMapper extends Mapper<CmsArticle> {
}
