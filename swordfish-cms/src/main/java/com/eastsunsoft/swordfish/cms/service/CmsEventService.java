package com.eastsunsoft.swordfish.cms.service;

import com.eastsunsoft.swordfish.cms.entity.CmsArticle;
import com.eastsunsoft.swordfish.common.ApiConstant;
import com.eastsunsoft.swordfish.common.SwordfishException;
import com.eastsunsoft.swordfish.common.event.SfEventOutCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 20/09/2017
 */
@Service
@Transactional
public class CmsEventService {
    @Autowired
    @Qualifier(SfEventOutCommon.OUT_COMMON)
    private MessageChannel output;

    @Async
    public void sendEvent(CmsArticle article) {
        System.out.println(Thread.currentThread().getName());
        try {
            output.send(MessageBuilder.withPayload(article).build());
        } catch (Exception e) {
            throw new SwordfishException(ApiConstant.ERRCODE_500);
        }
    }
}

