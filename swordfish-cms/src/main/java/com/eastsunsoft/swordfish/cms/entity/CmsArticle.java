package com.eastsunsoft.swordfish.cms.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <h1>CmsArticle</h1>
 * </br>
 *
 * @author donny
 * @date 20/09/2017
 */
@Table(name = "sf_cms_article")
public class CmsArticle {
    @Id
    private Long id;

    @Column(name = "title")
    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CmsArticle{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
