package com.eastsunsoft.swordfish.cms.service;

import com.eastsunsoft.swordfish.cms.entity.CmsArticle;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 20/09/2017
 */
public interface CmsArticleService {
    void save(CmsArticle article);
}
