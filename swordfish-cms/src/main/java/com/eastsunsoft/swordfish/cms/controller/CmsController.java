package com.eastsunsoft.swordfish.cms.controller;

import com.eastsunsoft.swordfish.cms.entity.CmsArticle;
import com.eastsunsoft.swordfish.cms.service.CmsArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <h1>ClassName</h1>
 * </br>
 *
 * @author donny
 * @date 14/09/2017
 */
@RestController
@RequestMapping("/")
public class CmsController {
    @Autowired
    private CmsArticleService cmsArticleService;


    @GetMapping("/test")
    public String test() {
       return "test";
    }

    @GetMapping("/send")
    public String send() {
//        output.send(MessageBuilder.withPayload("hello,world!").build());
        CmsArticle cmsArticle = new CmsArticle();
        cmsArticle.setId(1L);
        cmsArticle.setTitle("测试1");
        cmsArticleService.save(cmsArticle);

        return "success";
    }
}
